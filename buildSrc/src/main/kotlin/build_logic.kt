val localRepoDir = "${System.getProperty("user.home")}/.m2/repository"

object ServerConfig {
    const val mainClass = "io.vertx.core.Launcher"
    const val mainVerticle = "org.example.webstack.server.Server"
}

object Version {
    const val shadow = "2.0.1"
    const val kotlin = "1.2.60"
    const val vertx = "3.4.2"
    const val webscene = "0.1"
}

object Dependency {
    const val vertxCore = "io.vertx:vertx-core:${Version.vertx}"
    const val vertxWeb = "io.vertx:vertx-web:${Version.vertx}"
    const val vertxKotlin = "io.vertx:vertx-lang-kotlin:${Version.vertx}"
    const val shadowPlugin = "com.github.jengelman.gradle.plugins:shadow:${Version.shadow}"
    const val websceneCoreJs = "org.webscene:webscene-core-js:${Version.webscene}"
    const val websceneCoreJvm = "org.webscene:webscene-core-jvm:${Version.webscene}"
}

object PluginId {
    const val shadow = "com.github.johnrengelman.shadow"
    const val kotlin2Js = "kotlin2js"
}
