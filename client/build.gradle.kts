@file:Suppress("PropertyName")

import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }

    dependencies {
        classpath(kotlin(module = "gradle-plugin", version = Version.kotlin))
    }
}

apply {
    plugin(PluginId.kotlin2Js)
}

repositories {
    maven { url = uri(localRepoDir) }
}

dependencies {
    "compile"(kotlin(module = "stdlib-js", version = Version.kotlin))
    "compile"(Dependency.websceneCoreJs)
}

val webDir = "${projectDir.absolutePath}/web"
val compileKotlin2Js by tasks.getting(Kotlin2JsCompile::class) {
    val fileName = "webstack-client.js"

    kotlinOptions.outputFile = "$webDir/js/$fileName"
    kotlinOptions.sourceMap = true
    doFirst { File("$webDir/js").deleteRecursively() }
}
val build by tasks
val assembleWeb by tasks.creating(Copy::class) {
    dependsOn("classes")
    configurations["compile"].forEach { file ->
        from(zipTree(file.absolutePath)) {
            includeEmptyDirs = false
            include { fileTreeElement ->
                val path = fileTreeElement.path
                path.endsWith(".js") && path.startsWith("META-INF/resources/") ||
                    !path.startsWith("META_INF/")
            }
        }
    }
    from(compileKotlin2Js.destinationDir)
    into("$webDir/js")
}

task<Sync>("deployClientToServer") {
    val destDir = "${projectDir.parent}/server/src/main/resources/public"
    dependsOn(compileKotlin2Js, assembleWeb)
    from(webDir)
    into(destDir)
}
