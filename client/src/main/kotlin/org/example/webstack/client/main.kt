package org.example.webstack.client

import org.example.webstack.client.page.HomePage
import org.example.webstack.client.page.WelcomePage
import org.webscene.core.dom.DomQuery
import kotlin.browser.window

// Handles bootstrapping the web client.

@Suppress("unused")
fun main(args: Array<String>) {
    window.onload = {
        if (DomQuery.pageId() == "home") HomePage()
        else if (DomQuery.pageId() == "welcome") WelcomePage()
        HttpStatus.OK.num
    }
}
