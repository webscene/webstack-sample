package org.example.webstack.client

import org.webscene.core.html.element.ParentHtmlElement

/**
 * Custom parent HTML element.
 * @author Nick Apperley
 */
class FruitList : ParentHtmlElement() {
    override var tagName
        get() = "ol"
        set(_) {}

    init {
        val fruit = arrayOf("Orange", "Kiwifruit", "Mango", "Apple")
        fruit.forEach { htmlElement("li") { +it } }
    }
}
